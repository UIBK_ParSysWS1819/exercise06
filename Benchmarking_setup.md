# Benchmarking setup

## General
- create a bash script for the benchmark
- if available, try different compilers
- use different amount threads (1, 2, 4, 8, 16, etc.) and see the relation between the amount of threads and the speedup
- run it multiple times (e.g. 7) and build mean or median
- for each algorithm increased amounts of problem sizes (see below)