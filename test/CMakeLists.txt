set(SOURCE_FILES AVL_tree_test.cpp)
add_executable(AVL_tree_test ${SOURCE_FILES})
set_target_properties(AVL_tree_test PROPERTIES COMPILE_FLAGS "-O0 -fopenmp" LINK_FLAGS "-fopenmp")
target_link_libraries(AVL_tree_test gtest gtest_main -lpthread)