#include <gtest/gtest.h>
#include "../source/node.h"
#include "../source/util.h"


TEST(Seq_Test, is_valid_test) {
    // create unsorted tree on purpose
    std::vector<unsigned int> values{ 5, 6, 7, 7 };
    Node root(values[0]);
    Node left_child(values[2]);
    Node right_child(values[1]);
    root.left_child = &left_child;
    root.right_child = &right_child;

    EXPECT_FALSE(root.is_valid(values));

    // create not unique tree on purpose
    root = Node(values[1]);
    left_child = Node(values[0]);
    right_child = Node(values[2]);
    root.left_child = &left_child;
    root.right_child = &right_child;
    Node addional(values[3]);
    root.right_child->right_child = &addional;

    EXPECT_FALSE(root.is_valid(values));

    // create tree with additional value in origin on purpose
    values = { 5, 6, 7, 10 };   // 10 is the value we don't insert in tree
    root = Node(values[1]);
    left_child = Node(values[0]);
    right_child = Node(values[2]);
    root.left_child = &left_child;
    root.right_child = &right_child;

    EXPECT_FALSE(root.is_valid(values));

    // create tree which is sorted, unique, and all origin values inserted, but not balance
    values = { 6, 7, 8 };
    root = Node(values[0]);
    right_child = Node(values[1]);
    root.right_child = &right_child;
    Node right_right_child(values[2]);
    root.right_child->right_child = &right_right_child;

    EXPECT_FALSE(root.is_valid(values));
}

TEST(Seq_Test, insert_test) {
    std::vector<unsigned int> values{ 1, 6, 5, 7, 6, 8, 5, 12 };

    Node root(values[0]);
    root.insert(values);

    EXPECT_TRUE(root.is_valid(values));
    EXPECT_EQ(root.value, 7);
    EXPECT_EQ(root.left_child->value, 5);
    EXPECT_EQ(root.right_child->value, 8);
}

TEST(Seq_Test, insert_test_multiple) {
    // run test multiple times and see if it always works
    int runs = 100;
    int size = 1024;
    for (int i = 1; i <= runs; i++) {
        std::vector<unsigned int> values_to_insert;
        fill_vector(values_to_insert, size);

        Node root(values_to_insert[0]); // add first node as root
        root.insert(values_to_insert);

        EXPECT_TRUE(root.is_valid(values_to_insert));
    }
}