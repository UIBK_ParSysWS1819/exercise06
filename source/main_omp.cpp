#include <iostream>
#include <vector>
#include <random>
#include <algorithm>
#include "node-omp.h"
#include "util.h"
#include "chrono_timer.h"

/*
 * useful links:
 * https://en.wikipedia.org/wiki/AVL_tree
 * https://www.geeksforgeeks.org/avl-tree-set-1-insertion/
 * rotations: https://www.hackerrank.com/challenges/self-balancing-tree/problem
 */

int main(int argc, char** argv) {
    int N = 64; // default

    if(argc == 2) {
        N = std::stoi(argv[1]);
    }

    int runs = 1;
    std::vector<long> times;
    for (int i = 1; i <= runs; i++) {
        std::vector<unsigned int> values_to_insert;
        fill_vector(values_to_insert, N);
        // print_values(values_to_insert);

        std::string str("Sequential AVL tree insert run " + std::to_string(i));

        ChronoTimer timer(str);
        Node root(values_to_insert[0]); // add first node as root
        root.insert(values_to_insert);
        long time = timer.getElapsedTime();

        times.push_back(time);

        root.print();
        std::cout << std::endl;

        bool tree_is_valid = root.is_valid(values_to_insert);
        if(tree_is_valid){
            std::cout << "Tree is valid" << std::endl;
        } else {
            std::cout << "Tree is not valid" << std::endl;
            return EXIT_FAILURE;
        }
    }

    std::cout << median(times) << std::endl;

    return EXIT_SUCCESS;
}