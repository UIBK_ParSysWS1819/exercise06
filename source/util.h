#ifndef EXERCISE06_UTIL_H
#define EXERCISE06_UTIL_H

#include <iostream>
#include <vector>
#include <random>
#include <algorithm>


void print_values(std::vector<unsigned int> &values) {
    // std::cout << "Values to insert in tree:" << std::endl;
    for(auto value : values) {
        std::cout << value << ", ";
    }
    std::cout << std::endl;
}

void fill_vector(std::vector<unsigned int> &values, int size) {
    std::random_device random_device;
    std::mt19937 engine(random_device());
    std::uniform_int_distribution<unsigned int> uniform_dist(0, size / 8);
    for(int i = 0; i < size; i++) {
        values.push_back(uniform_dist(engine));
    }
}

long median(std::vector<long> times) {
    std::sort(times.begin(), times.end());

    int size = times.size();

    if(size % 2 == 0) { // if the size is even, build the average of the middle two.
        long val1 = times[size / 2 - 1];
        long val2 = times[size / 2];

        return (val1 + val2) / 2l;
    }
    else {
        return times[size / 2];
    }
}

#endif //EXERCISE06_UTIL_H
