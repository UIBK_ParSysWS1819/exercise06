#!/bin/bash

#$ -N AVL_test

#$ -q std.q

#$ -cwd

#$ -o output_AVL.txt

#$ -j yes

#$ -pe openmp 8

./bench_AVL.sh
./bench_AVL_icc.sh
