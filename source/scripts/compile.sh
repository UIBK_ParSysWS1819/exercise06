#!/bin/bash

echo 'compiling gcc'

module load gcc/8.2.0
g++ -std=c++11 -O3 ../main_seq.cpp -o ../main_seq
#g++ -std=c++11 -O3 -fopenmp ../main_omp.cpp -o ../main_omp
module unload gcc/8.2.0

echo 'compiling icc'

module load intel/15.0
icpc -std=c++11 -O3 ../main_seq.cpp -o ../main_seq_icc
#icpc -std=c++11 -O3 -fopenmp ../main_omp.cpp -o ../main_omp_icc
module unload intel/15.0