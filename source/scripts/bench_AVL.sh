#!/bin/bash


#printf "|Size|Seq|1T|2T|4T|8T|\n"
#printf "|---:|---:|---:|---:|---:|---:|\n"
printf "|Size|Seq|\n"
printf "|---:|---:|\n"

for size in 100 1000 5000 10000 20000 30000
do
    printf "|%s|" $size

    out_seq=`../main_seq $size`
    printf "%s ms|" $out_seq

    # parallel version not done this time
    #for threads in 1 2 4 8
    #do
    #    export OMP_NUM_THREADS=$threads
    #    out_par=`../main_omp $size`
    #    printf "%s ms|" $out_par
    #done
    printf "\n"

done
