struct Node {
    unsigned int value;
    Node* left_child;
    Node* right_child;
    int height;
    int balance;

    Node(unsigned int val): value(val), left_child(nullptr), right_child(nullptr), height(1), balance(0){}

    void print(){
        if(left_child){
            left_child->print();
        }
        std::cout << " " << value << "(" << height << "," << balance << ")" << " ";
        //std::cout << " " << value << " ";
        if(right_child){
            right_child->print();
        }
    }

    void calculate_height() {
        if (right_child) {
            right_child->calculate_height();
        }
        if (left_child) {
            left_child->calculate_height();
        }
        int left_height = left_child ? left_child->height : 0;
        int right_height = right_child ? right_child->height : 0;
        //height = left_height > right_height ? left_height + 1 : right_height + 1;
        height = std::max(left_height,right_height) + 1;
        balance = left_height - right_height;
    }

    void right_left_rotation(){
        Node* right = right_child;
        Node* right_left = right->left_child;
        Node* right_left_right = right_left->right_child;

        right_child = right_left;
        right_left->right_child = right;
        right->left_child = right_left_right;

        right_right_rotation();

        //calculate_height(); // no need, since right_right resets the hight
    }

    void right_right_rotation(){
        Node* right = right_child;
        Node* right_right = right->right_child;
        Node* right_left = right->left_child;

        std::swap(value, right->value);

        right->left_child = left_child;
        right->right_child = right_left;
        left_child = right;
        right_child = right_right;

        calculate_height();
    }

    void left_right_rotation(){
        Node* left = left_child;
        Node* left_right = left->right_child;
        Node* left_right_left = left_right->left_child;

        left_child = left_right;
        left_right->left_child = left;
        left->right_child = left_right_left;

        left_left_rotation();

        //calculate_height(); // no need, since right_right resets the hight
    }

    void left_left_rotation(){
        Node* left = left_child;
        Node* left_left = left->left_child;
        Node* left_left_right = left->right_child;

        std::swap(value, left->value);

        left->left_child = left_left_right;
        left->right_child = right_child;
        left_child = left_left;
        right_child = left;

        calculate_height();
    }

    void insert(std::vector<unsigned int> node_list) {
        for (unsigned int i = 1; i < node_list.size(); i++) {
            // std::cout << "Adding: " << value << std::endl;
            Node* to_add = new Node(node_list[i]);
            insert(to_add);
        }
    }

    void insert(Node *n) {
        //std::cout << "Add " << n->value << std::endl;
        if(n->value > value) {
            if(right_child) {
                right_child->insert(n);
            } else {
                right_child = n;
            }
        } else if(n->value < value) {
            if(left_child) {
                left_child->insert(n);
            } else {
                left_child = n;
            }
        } else {
            // value already exists
            return;
        }
        calculate_height();
        if (balance < -1){
            if (right_child->balance < 0) {
                //std::cout << "RR" << std::endl;
                //std::cout << value << std::endl;
                right_right_rotation();
            } else if(right_child->balance > 0) {
                //std::cout << "RL" << std::endl;
                //std::cout << value << std::endl;
                right_left_rotation();
            } else {
                std::cout << "ELSE R" << std::endl;
                //std::cout << value << std::endl;
            }
        }
        else if (balance > 1) {
            if (left_child->balance < 0) {
                //std::cout << "LR"<< std::endl;
                //std::cout << value << std::endl;
                left_right_rotation();
            } else if(left_child->balance > 0) {
                //std::cout << "LL" << std::endl;
                //std::cout << value << std::endl;
                left_left_rotation();
            } else {
                std::cout << "ELSE L" << std::endl;
                //std::cout << value << std::endl;
            }
        }
    }

    std::vector<int> get_values(){
        std::vector<int> items;
        if(left_child){
            auto left_items = left_child->get_values();
            items.insert(items.end(), left_items.begin(), left_items.end());
        }
        items.push_back(value);
        if(right_child){
            auto right_items = right_child->get_values();
            items.insert(items.end(), right_items.begin(), right_items.end());
        }
        return items;
    }


    bool is_valid(std::vector<unsigned int> items_to_be_inserted) {
        std::vector<int> node_items = get_values();

        // check if sorted
        if (!std::is_sorted(node_items.begin(), node_items.end())) {
            std::cout << "Tree is not sorted" << std::endl;
            return false;
        }

        // check if unique values
        auto unique =  std::unique(node_items.begin(), node_items.end());
        if(unique != node_items.end()){
            std::cout << "Tree contains duplicates" << std::endl;
            return false;
        }

        // check if contains all from origin values
        for(auto val: items_to_be_inserted){
            if(!(std::find(node_items.begin(), node_items.end(), val) != node_items.end())) {
                std::cout << "Does not contain " << val << std::endl;
                return false;
            }
        }

        // check if balanced
        calculate_height();
        if (balance > 1 || balance < -1){
            std::cout << "Tree is not balanced" << std::endl;
            return false;
        }

        return true;
    }
};