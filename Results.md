# AVL Tree
Unfortunately we were only able to produce a sequential version of the
AVL tree insert problem.

We tried a variety of different approaches for the parallel version, but
either they didn't produced faster code (or code nearly as fast as the sequential version)
or produced incorrect AVL trees / led to segmentation errors.

Our ideas were the following:
- Each thread takes a piece of the random numbers and builds its own tree. At the end the trees
would then be merged sequentially. This solution probably does break the guidelines, that the insert
operation should be parallelized and not the whole problem.
- Split the insert into a lookup and an actual insert. Since with the problem we have a lot of duplicates,
we can make the search in parallel and only have one thread do the insert. That means that a lot of threads can just
look inside the tree, without breaking any branches, while only one thread does actual inserting. Although we
did manage to produce a kind of working version with this idea, the times were way slower then the sequential versions
(more then 10x slower locally), and by adding more threads, we wouldn't get any speedup.
- Doing something similar, but in a master - slave kind of way. This did lead to deadlocks.
- Using locks to lock part of the tree when an insert is happening. This was really complicated and lead to deadlocks.


## Benchmark results
Since we only got a functional sequential version working, here are the results for that one for gcc and icc on the cluster:

##### gcc:

|Size|Seq|
|---:|---:|
|100|0 ms|
|1000|1 ms|
|5000|49 ms|
|10000|251 ms|
|20000|1089 ms|
|30000|2519 ms|

##### icc:

|Size|Seq|
|---:|---:|
|100|0 ms|
|1000|2 ms|
|5000|62 ms|
|10000|292 ms|
|20000|1271 ms|
|30000|2891 ms|

So icc seems to be a bit slower than gcc, when executing our sequential version on the cluster.